<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('party_id');
			$table->unsignedInteger('location_id');
            $table->string('title');
            $table->string('querystring');
            $table->text('description');
			$table->string('information_url')->nullable();;
			$table->string('ticket_url')->nullable();;
            $table->timestamp('from')->nullable();
            $table->timestamp('until')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
