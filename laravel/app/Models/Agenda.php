<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class Agenda extends Model
{
    use SoftDeletes;
	
	public function scopeOverview($query)
	{
		return $query
			->where('from', '>', Carbon::now())
			->orderBy('from', 'desc');
	}
}
