<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use Illuminate\Support\Str;

class News extends Model
{
    use SoftDeletes;

    protected $dates = ['show_at', 'deleted_at'];

    protected $fillable = [
        'title', 'querystring', 'intro', 'message', 'show_at'
    ];

    protected $casts = [
        // 'show_at' => 'datetime:Y-m-d\TH:i'
    ];

	public function images()
	{
		// return $this->hasManyThrough('App\Image', 'App\NewsImage');
		return $this->belongsToMany('App\Image');
	}

    protected static function boot()
    {
        parent::boot();

        self::saving(function($model){
            $model->querystring = Str::slug($model->title);
        });
    }

	public function scopeOverview($query)
	{
		return $query
			->with('images')
			->where('show_at', '<=', Carbon::now())
			->orderBy('show_at', 'desc');
	}

    public function setShowAtAttribute($value)
    {
        $this->attributes['show_at'] = Carbon::createFromTimestamp(strtotime($value));
    }

    public function getEditShowAtAttribute($value)
    {
        return Carbon::parse($value)->format('Y-m-d\TH:i');
    }
}
