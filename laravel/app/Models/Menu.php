<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Menu extends Model implements HasMedia
{
    use SoftDeletes, InteractsWithMedia;
    //

	public function pages()
	{
		// return $this->hasManyThrough('App\Image', 'App\NewsImage');
		return $this->hasMany('App\Page');
	}
	
	public function registerMediaConversions(Media $media = null): void
	{
		$this->addMediaConversion('thumb')
        	->fit(Manipulations::FIT_CROP, 300, 300)
			->nonQueued();
	}
	
	public function registerMediaCollections(): void
	{
		$this->addMediaCollection('main')->singleFile();
		$this->addMediaCollection('my_multi_collection');
	}
}
