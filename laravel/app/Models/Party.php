<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str; 

class Party extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'description'
    ];

    protected static function boot()
    {
        parent::boot();

        self::saving(function($model){
            $model->querystring = Str::slug($model->name);
        });
    }
}
