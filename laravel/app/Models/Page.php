<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{
    use SoftDeletes;
    //

	public function menus()
	{
		// return $this->hasManyThrough('App\Image', 'App\NewsImage');
		return $this->belongsToMany('App\Menu');
	}
}
