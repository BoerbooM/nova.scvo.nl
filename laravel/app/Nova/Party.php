<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Waynestate\Nova\CKEditor4Field\CKEditor;

class Party extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Party::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
	public static $search = [
		'id', 'name', 'description'
	];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            ID::make()
                ->hideFromIndex()
                ->hideFromDetail(),

			Text::make('Naam', 'name')
				->sortable()
				->rules('required', 'max:255'),

            Select::make('Type', 'type')->options([
                'club' => 'Club',
                'kapel' => 'Kapel',
                'cafe' => 'Café',
                'overig' => 'Overig'
            ])->rules('required'),

			CKEditor::make('Omschrijving', 'description')
				->showOnPreview()
				->alwaysShow(),

			Text::make('Website')
                ->rules('max:255')
                ->sortable()
                ->hideFromIndex(),

			Text::make('Facebook')
                ->rules('max:255')
				->sortable()
                ->hideFromIndex(),

			Text::make('Twitter')
                ->rules('max:255')
                ->sortable()
                ->hideFromIndex(),

			Text::make('Instagram')
                ->rules('max:255')
                ->sortable()
                ->hideFromIndex()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
