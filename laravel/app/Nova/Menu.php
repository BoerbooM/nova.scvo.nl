<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;

class Menu extends Resource
{
	/**
	 * The model the resource corresponds to.
	 *
	 * @var string
	 */
	public static $model = \App\Models\Menu::class;

	/**
	 * The single value that should be used to represent the resource when being displayed.
	 *
	 * @var string
	 */
	public static $title = 'name';

	/**
	 * The columns that should be searched.
	 *
	 * @var array
	 */
	public static $search = [
		'id', 'name', 'url'
	];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Content';

	/**
	 * Get the fields displayed by the resource.
	 *
	 * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
	 * @return array
	 */
	public function fields(NovaRequest $request)
	{
		return [
			ID::make()->sortable(),

			Select::make('Parent', 'parent_id')
				->searchable()
				->options(self::parents($request))
				->displayUsingLabels()
				->default(0),

			Text::make('Name')
				->sortable()
				->rules('required', 'max:255'),

			Text::make('URL')
				->sortable()
				->creationRules('required', 'unique:menus,url')
				->updateRules('required', 'unique:menus,url,{{resourceId}}'),

			Images::make('Thumbnail', 'thumbnail')
				->conversionOnIndexView('thumb') // conversion used to display the image
				->conversionOnForm('thumb') // conversion used to display the image on the model's form
				->rules('required'), // validation rules
		];
	}
	
	/**
	 * Get the cards available for the request.
	 *
	 * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
	 * @return array
	 */
	public function cards(NovaRequest $request)
	{
		return [];
	}

	/**
	 * Get the filters available for the resource.
	 *
	 * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
	 * @return array
	 */
	public function filters(NovaRequest $request)
	{
		return [];
	}

	/**
	 * Get the lenses available for the resource.
	 *
	 * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
	 * @return array
	 */
	public function lenses(NovaRequest $request)
	{
		return [];
	}

	/**
	 * Get the actions available for the resource.
	 *
	 * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
	 * @return array
	 */
	public function actions(NovaRequest $request)
	{
		return [];
	}

	private function parents(NovaRequest $request)
	{
		$menu = Menu::whereNot('id', $request->resourceId)->get();

		$arr = [];
		foreach ($menu as $item){
			$arr[$item->id] = $item->name;
		}

		return $arr;
	}
}
